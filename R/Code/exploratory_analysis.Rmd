---
title: "exploratory_analysis"
author: "Michael Sieler"
date: "10/5/2021"
output: html_document
---

## Setup environment

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

# Set working directory
setwd("~/Dropbox/My Mac (Michaels-MacBook-Pro.local)/Documents/School/R_tutorials/code-club")


# Saves the project path as the working directory from where this project is saved
#proj.path <- getwd()
proj.path <- "~/Dropbox/My Mac (Michaels-MacBook-Pro.local)/Documents/School/R_tutorials/code-club"


# Saves path where data files are stored
data.path <- paste0(proj.path, "/Data")

# Saves path where output files will be saved
output.path <- paste0(proj.path, "/Results")

# Load Libraries
source(paste0(proj.path,"/Code/HelperScripts/libraries.R"))

# Load Functions
source(paste0(proj.path,"/Code/HelperScripts/functions.R"))

# Load Plot Settings
source(paste0(proj.path,"/Code/HelperScripts/plot_settings.R"))

# Set seed
user.seed <- 42 # seed for replication, number is arbitrary
set.seed(user.seed)

```


```{r import-data}


data <- read_excel(paste0(proj.path,"/Data/Raw/NFL_Combine_Dataset.xlsx"))
View(data)   


```



```{r}

var <- c(1,5,6,7,8, 10, 11, 12, 13)

# player a
a <- as.matrix( data[1, var])



# player b
b <- as.matrix(data[4, var])



#define Jaccard Similarity function
jaccard <- function(a, b) {
    intersection = length(intersect(a, b))
    union = length(a) + length(b) - intersection
    return (intersection/union)
}

#find Jaccard Similarity between the two sets 
jaccard(a, b)


```



```{r}

library(vegan)
  count_data <- position_data %>%
  select(median_40, median_bench, median_weight)
nfl_metadata <- position_data %>%
  select(position, Year)

nfl_dist <- vegdist(count_data, method="jaccard")
hist(nfl_dist)


```



















